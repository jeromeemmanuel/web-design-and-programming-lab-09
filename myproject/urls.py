from django.urls import path
from myapp import views


urlpatterns = [
    path('', views.home, name='home'),
    path('api/likedbooks/', views.book_like, name='like'),
    path('api/unlikedbooks/',views.book_unlike, name='unlike'),
    path('topbook/',views.topbook, name='topbook')
]
