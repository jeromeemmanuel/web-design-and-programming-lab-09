# Story 9
This Gitlab repository is made by **Jerome Emmanuel**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/jeromeemmanuel/web-design-and-programming-lab-09/badges/master/pipeline.svg)](https://gitlab.com/jeromeemmanuel/web-design-and-programming-lab-09/commits/master)
[![coverage report](https://gitlab.com/jeromeemmanuel/web-design-and-programming-lab-09/badges/master/coverage.svg)](https://gitlab.com/jeromeemmanuel/web-design-and-programming-lab-09/commits/master)

## URL
To access the website, go to [https://story9-jerome.herokuapp.com](https://story9-jerome.herokuapp.com)